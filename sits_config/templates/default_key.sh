# this is the template file a key_*.sh file

# initially the data in key files are taken from the keys explicitly set in the
# context file. Any unset keys are given a blank key file when you first use it.
# the command attr is a bash command or function to be executed when 'KEY' is 
# pressed. It can either be a simple command like 'declare -F' or, more commonly,
# it can be function you've defined either in this file or elsewhere.
declare -Ag _CONTEXT_KEY
_CONTEXT_KEY[type]="command"
_CONTEXT_KEY[label]="TEXT"
_CONTEXT_KEY[command]="TEXT"
_CONTEXT_KEY[help]="TEXT"

#function my_default_function {
#  echo "hello world, I'm the default function"
#}
