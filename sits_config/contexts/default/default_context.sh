#!/usr/bin/env bash
# -a indexed array, -A associative array
# the _default_keys array is a list of shortcut labels for use in this context.
# by default, it's lower case letters a - t, but if you wish it could a - z, or even a mix 
# of words and letters like this: (a b con del edit funcs g help i j k) or
# (a A b B c C) 
# the only restriction is that keys must be characters that can also be part of a valid bash 
# variable, so (a b $ @ etc) would not work
declare -ag _default_keys=(a b c d e f g h i j k l m n o p q r s t)

# builtins (from sits.ml) are listed, but currently the array isn't used for anything 
# internally, but it's provided to ease validating that all builtin commands are present
# in your key set
declare -ag _default_builtins=(sits_context sits_delete_command sits_edit_command sits_help)
# type, label, command, and help must be listed in the attributes array. this is provided
# if you want to add your own custom attributes 
declare -ag _default_attrs=(type label command help)

# default keys follow the format _default_? where ? corresponds to a character or word in
# the _default_keys array
# Keys of type builtin are core commands located in sits.ml
# key 'f' contains 'declare', which is a standard bash command for declaring variables
# in this case, the -F flag prints all the function names set for your environment
declare -Ag _default_c=( [type]="builtin" [label]="show/add/switch context" [command]="sits_context" ) 
declare -Ag _default_d=( [type]="builtin" [label]="delete commands" [command]="sits_delete_command" ) 
declare -Ag _default_e=( [type]="builtin" [label]="edit commands" [command]="sits_edit_command" ) 
declare -Ag _default_f=( [type]="command" [label]="show shell functions" [command]="declare -F" ) 
declare -Ag _default_h=( [type]="builtin" [label]="show help entry for commands" [command]="sits_help") 

# set global variables here for use in this context if you wish
# declare -g some_var_foo

# variables to unset when you switch to a different context
# This might be useful unsetting large functions or variables which have a different 
# meaning in a different context.
declare -ag _default_unset_vars=(_default_keys)

