# default values for key e
declare -Ag _default_e
_default_e[type]="builtin"
_default_e[label]="edit command"
_default_e[command]="sits_edit_command"
_default_e[help]="edit a command with the text editor defined by \$EDITOR"

