#!/usr/bin/env bash
# Author: Citizen Eight <c8temp@protonmail.com>
# _sits_* functions are internal/"private", sits_* (no leading underscore) are
# external/"public" functions
#shopt -s expand_aliases #allows aliases in this shell instead of sub shell
#shopt -s extglob # allows case to use +()

function _valid_sits_cmd {
  local -i valid_cmd=1
  if [[ $1 =~ ^[a-zA-Z]+$ ]]; then
    valid_cmd=0
  fi
  echo $valid_cmd
}

function _sits_set_context {
  if [ -z $1 ] || [ ! -d $sits_contexts_dir/$1 ]; then
    msg "warn" "could not switch to context $1"
    sits_context_val="default"
    return;
  fi
  sits_context_val=$1
  echo $sits_context_val > $sits_config_dir/sits_context_val
  _sits_edit_context_file
  _sits_load_commands
}

function _sits_edit_context_file {
  local new_context_file=$sits_contexts_dir/$sits_context_val/${sits_context_val}_context.sh
  if [ ! -e $new_context_file ]; then #if file does not exist
    cp ${sits_config_dir}/templates/default_context.sh $new_context_file
    sed -i "s/CONTEXT/$sits_context_val/g" $new_context_file
  fi
  if [ "$1" = "user_edit" ]; then
    `echo "$EDITOR $new_context_file"`
  fi
  source $new_context_file
}

function _sits_load_project {
  msg "G" "current project: $sits_project_val" "!"
  if test -f *.project.rec ; then
    msg "B" "project files found:"
    # look up one dir for project files, no need to go further
    readrec "recsel -p Context journal.project.rec"
    msg "warn" "yo $context"
    ls -lah *.project.rec
  else 
    msg "warn" "no project files found"
  fi 
  pwd
}
function _sits_load_commands {
  declare -n key_array=_${sits_context_val}_keys
  declare -ag key_array
  msg "G" "loading commands for context: $sits_context_val"
  _sits_load_project
  local key_files=$sits_contexts_dir/$sits_context_val
  source $key_files/${sits_context_val}_context.sh
  local letter
  for letter in ${key_array[@]}; do
    declare -n _key=_${sits_context_val}_${letter}
    declare _file=$key_files/_${sits_context_val}_${letter}.sh
    # if no file is present and the _context_key[] var is set, copy it
    # from the default_key file and insert var data
    if [[ ! -e $_file  && ! -z ${_key[label]} ]]; then
      local -n label=_${sits_context_val}_${letter}[label]
      msg "G" "$letter : setting key with \"$label\""
      cp ${sits_config_dir}/templates/default_key.sh $_file
      sed -i "s/KEY/$letter/g" $_file
      sed -i "s/CONTEXT/$sits_context_val/g" $_file
      declare -n attrs="_${sits_context_val}_attrs"
      declare -ag attrs
      for _attr in ${attrs[@]}; do
        key="_${sits_context_val}_${letter}\[$_attr\]"
        val="${_key[$_attr]}"
        sed -i "/$key/c $key=\"$val\"" $_file
      done
    fi
    if [[ -e $_file ]]; then
      source $_file
    fi
  done
}

function sits_help {
  if [ -z $1 ]; then
    msg "type " "cmd" "help KEY" "for help "
    msg "example: " "cmd" "help e"
  else
    msg "title" $1 key
    local -n key=_${sits_context_val}_$1
    # cmd_file=$sits_contexts_dir/$sits_context_val/key_$1.sh
    if [[ ! -z ${key[help]} ]]; then
      msg "${key[help]}"
    else
      msg "no help text found for " "blue" $1
    fi
  fi
}

function sits_context {
  if [ -z $1 ]; then
    msg "blue" "context commands: a/add CONTEXT, d/delete CONTEXT, e/edit CONTEXT, s/switch-to CONTEXT"
    msg "blue" "example: ml context s default"
    local context_dirs=($(ls -d $sits_contexts_dir/*/))
    local title=""
    for contexts in "${context_dirs[@]}"; do
      title=$(basename $contexts)
       if [[ "$title" == "$sits_context_val" ]]; then
         msg "G" ">" "$title"
       else
         msg  $title
       fi
    done
  else
    if [ -z $2 ]; then
        msg "warn" "missing CONTEXT, ex: ${1} default"
        return;
    fi
    if [ ! -z $2 ]; then
      case $1 in
	"a" ) ;&
	"add" ) mkdir $sits_contexts_dir/$2
	        if [ $? -ne 0 ]; then
		  msg "warn" "couldn't make context. $2"
		else
                  _sits_set_context $2
		  _sits_edit_context_file "user_edit"
		  sits_context switch-to $2
		fi
		;;
	"d" ) ;&
	"delete" ) msg "to delete, rm -r $sits_contexts_dir/$2"
		   msg "be sure to backup anything here you wish to save ";;
	"e" ) ;&
	"edit" ) local current_context=$sits_context_val
                 _sits_set_context $2
		 _sits_edit_context_file  "user_edit"  ;;
	 "s" ) ;&
	"switch-to" )
	  msg "G" $2
          _sits_set_context $2
	  #_sits_load_commands
	;;
	* ) msg "warn" "$1 invalid command for context"
	      ;;
      esac
    fi
  fi
}

function sits_show_commands { #default no key
  # TODO: render output to text file, re-render when
  # a command is edited.
  local output="context: $sits_context_val"
  output+=`msg "green" "command" "blue" "text" "red" "unknown"`
  case $1 in
	  "" )
	       local _key
	       declare -n key_array=_${sits_context_val}_keys
	       for _key in "${key_array[@]}"
	       do
                 local -n my_key=_${sits_context_val}_${_key}
		 local format="blue"
		 local text=${my_key[label]}
	         case ${my_key[type]} in
	           "" ) format="blue" ;;
	           "command" ) format="green" ;;
	           "context" ) format="red" text="switch to context: ";;
	           "mod" ) format="Y" ;;
	           "notes" ) format="B" ;;
	         esac
		 output+=`msg "blue" "\n ${_key} "`
		 output+=`msg $format "${text}"`
		 output+=`msg "green" ${my_key[command]} `
	       done
	 echo "$output"
	 ;;
    * )
	 local -n key_info=_${sits_context_val}_$1
         if [[ "$1" =~ . ]]; then #add single letter regex rule
            msg "blue" $1
	    msg "G" "type ${key_info[type]}"
	    msg "B" "label: ${key_info[label]}"
	    msg "Y" "command: ${key_info[command]}"
         fi
       ;;
  esac
}

function sits_edit_command { #default e
  if [[ "$(_valid_sits_cmd $1)" == 1 ]]; then
    msg "warn" "this can't be used as a command"
    return;
  fi
  cmd_file=$sits_contexts_dir/$sits_context_val/_${sits_context_val}_$1.sh
  if [ ! -e $cmd_file ]; then #if file does not exist
    cp ${sits_config_dir}/templates/default_key.sh $cmd_file
    sed -i "s/KEY/$1/g" $cmd_file
    sed -i "s/CONTEXT/$sits_context_val/g" $cmd_file
  fi
  `echo "$EDITOR $cmd_file"`
  # append keys array unless key_$ already present
  if [[ ! "${sits_keys[@]}" =~ "$1" ]]; then
    sits_keys+=("$1")
  fi
  msg "blue" "edited '$1' key at $cmd_file"
  source $cmd_file # missing from load_commands?
}

function sits_delete_command { #default d
  if [[ -z $1 ]]; then
    msg "warn" "delete what?"
  else
    cmd_file=$sits_contexts_dir/$sits_context_val/_${sits_context_val}_$1.sh
    msg "warn" "deleting $cmd_file"
    rm $cmd_file
    local -n key=_${sits_context_val}_$1
    msg "warn" "unsetting ${key[@]}"
    unset key
  fi
}

function sits {
  if [ "$sits_context_val" = "" ]; then
    sits_context_val=$(cat $sits_config_dir/sits_context_val)
  fi
  case $1 in
    "" ) sits_show_commands  ;;
    "context" ) shift ; sits_context "$1" ;;
    "edit" ) shift ; sits_edit_command "$1" ;;
    "functions" ) declare -F ;;
    "load" ) _sits_load_commands ;;
    "show" ) shift ; sits_show_commands $1;;
    *      ) local -n key=_${sits_context_val}_$1
	     # exit if user input is not found in $keys array
	     declare -n key_array=_${sits_context_val}_keys
	     if [[ ! "${key_array[@]}"  =~ "${1}" ]]; then
               msg "warn" "invalid choice $1"
	       msg "blue" "you can use these keys in this context: ${sits_keys[@]},"
	       msg "blue" "or these these core commands: ${sits_builtins[@]}"
	       return;
	     fi
	     local key_type=${key[type]}
	     # if key is unset/blank, open editor
	     if [[ -z $key_type ]]; then
               sits_edit_command "$1"
	       return;
	     fi
	     shift
	     case $key_type in
	       "context" ) sits_context "s" "${key[command]}"  ;;
               "command" ) msg "blue" "${key[command]} $@ "
		           history -s ${key[command]} $@ ;& # fall through to builtin
	       "builtin" ) `echo "\${key[command]} ${@}"` ;;
	       * ) `echo "\${key[command]} ${@}"` ;;
	     esac
  esac
}
if [[ -z $init_once ]]; then
  source vars.sh
  sits_context_val=$(cat $sits_config_dir/sits_context_val)
  source util.sh #load msg function
  _sits_load_commands
  declare -i init_once=1
fi
sits ${@}
