Script in the Shell
===
SitS is a bash shell script for managing user created scripts and bash functions
on your Linux/mac/BSD desktop or server. It allows you to assign scripts to single letter 
shortcuts and to organize them into groups. This is a tool for CLI users who do some actions so
much it's an annoyance and other actions so infrequently they
need to look it up every time.

Example Use
---
![animation of adding a function in SitS](demo.gif "a demo gif")
Instead of typing `urxvt -fn "xft:Terminus (TTF):pixelsize=24"` to launch a urxvt terminal,
this command could be saved into a SitS context so that one may simply type `f t`. In this 
example `f` is an alias of `sits.sh` and `t` runs a function stored in `../contexts/default/_default_t.sh`.
Running `f` without any arguments will list out all stored functions.
```
context: default
 a force system clock update
 b toggle external monitor
 ...
 q download youtube video from open tab
 t launch urxvt terminal

```
Installation 
---
Edit these values in vars.sh 
```
sits_install_dir=""
sits_config_dir="" 
sits_contexts_dir="" # where "contexts" are stored
sits_library_dir="$HOME/scripts" # not used yet
```

Temporarily uncomment the "unset install_flag" line before
running

> . setup.sh

Make sure your installation dir is set in your \$PATH variable.
You'll probably want to add a single letter alias (ex: `alias f='source sits.sh'
to `.bashrc`

Run the alias to confirm everything is installed correctly. You should see
a list of letters `a` through `t` with letters c, d, e, f, and h with commands
described next to those letters. These are the default commands which are used
for managing contexts, editing and deleting commands, showing the functions set
in this shell, and viewing the optional help entries. 

To create a new command, pick one of those blank letters and use the `edit` command
to launch your `$EDITOR` with the text file that corresponds to that letter. example: `f e a`
will open a file with this contents
```
declare -Ag _default_a
_default_a[type]="command"
_default_a[label]="TEXT"
_default_a[command]="TEXT"
_default_a[help="TEXT"

#function my_default_function {
#  echo "hello world, I am the default function"
#}
```
Uncomment `my_default_function` and change the command var line to `_default_a[command]="my_default_function"`
Now running `f a` will print hello world.

What are contexts?
---
Contexts are the organizational structure for the functions/scripts managed by
SitS. For example, you could have a context named "maintenance" where package
management and backup automation scripts are stored while another context named
"coding" where version control and deployment scripts are stored. 

How it works
---
  * util.sh
  * sits.sh 

util.sh
---
provides these functions
* msg: convenient wrapper for colorizing echo messages
* add_temp_globals: add names of variables to a global array for unsetting later
* unset_temp_globals: unset variables. 

sits.sh
---
A script organizer for efficient manual operation of a linux box.
When you run it for the first time with no arguments, sits shows the default 
context, or set of user defined commands. My bash alias for sits.sh is "f",
so to 
* create short key sequences which act like aliases.
* save key sequences in sets (contexts)
<add gif screenshot>

Dependencies
===
scripts in this collection are compatible with GNU bash 4.4.23 and some scripts in the 
library use command line utilities which must be installed separately or replaced with
something specific to your OS

fixes/features
===
* make sure $editor is set
* CRUD the library scripts

Credits
===
Author: Citizen Eight <c8temp@protonmail.com> Jan. 2019
