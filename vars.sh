#!/usr/bin/env bash
# Author: Citizen Eight <c8temp@protonmail.com>
declare -g sits_install_dir="$HOME/_p/sits"
declare -g sits_config_dir="$HOME/.config/sits"
declare -g sits_contexts_dir="$HOME/.config/sits/contexts"
declare -g sits_library_dir="$HOME/scripts"
declare -g sits_context_val #=$(cat $sits_config_dir/sits_context_val)

declare -g install_flag=1
# uncomment the following line before running setup.sh
# unset install_flag
