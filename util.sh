#!/usr/bin/env bash
# Author: Citizen Eight <c8temp@protonmail.com>

# msg(style, text) 
# take N strings to format with pre-defined color/style codes.
# ex:
# msg "title" "the title" "red" "this is red text" 
function msg {
  OUTPUT="\e[0m"
  while ((  "$#" )); do
    case $1 in
      "red"    )  OUTPUT="$OUTPUT\e[33m" ;;
      "R"      )  OUTPUT="$OUTPUT\e[31m" ;;
      "fail"   )  OUTPUT="$OUTPUT\e[1m\e[31m" ;; # bold, red
      "green"  )  OUTPUT="$OUTPUT\e[32m" ;;
      "G"      )  OUTPUT="$OUTPUT\e[32m" ;;
      "success")  OUTPUT="$OUTPUT\e[32m" ;;
      "yellow" )  OUTPUT="$OUTPUT\e[1;33m" ;;
      "Y"      )  OUTPUT="$OUTPUT\e[33m" ;;
      "warn"   )  OUTPUT="$OUTPUT\e[33m" ;;
      "blue"   )  OUTPUT="$OUTPUT\e[34m" ;;
      "B"      )  OUTPUT="$OUTPUT\e[34m" ;;
      "cmd"    )  OUTPUT="$OUTPUT\e[34m" ;; # blue
      "title"  )  OUTPUT="$OUTPUT\e[1m\e[35m"  ;; # bold, magenta
      "test"   )  OUTPUT="$OUTPUT\e[1m\e[12m"  ;; # bold, magenta
      " "      )  OUTPUT="$OUTPUT\e[0m"  ;; # default
      "_"      )  OUTPUT="$OUTPUT\e[0m"  ;; # default
      *        )  OUTPUT="$OUTPUT $1" ;;
    esac
    shift
  done 
  echo -e "$OUTPUT \e[0m" # restore defaults
}

function add_temp_globals {
  if [ -z ${unload_vars} ]; then
    declare -ag unload_vars
  fi
  if [ ! -z $1 ] && [ ! "$1" = "" ] && [[ ! "${unload_vars[@]}" =~ "$1" ]]; then
    unload_vars+=($1)
  else
    msg "warn" "either blank, not given, or already in unload_vars array"
  fi
}

function unset_temp_globals {
  if [ ! -z ${unload_vars} ]; then
    local uv
    for uv in ${unload_vars[@]}; do
      local -n unset_var=$uv
      unset unset_var
    done
  fi
e
#/bin/bash

# msg(style, text) 
# take N strings to format with pre-defined color/style codes.
# ex:
# msg "title" "the title" "red" "this is red text" 

REPL_RETURN=''
function repl { opts=$1; repeat=$2
  while read -r -p "$ " && [[ $REPLY != q ]] && [[ $REPLY =~ [^$@] ]]; do # -n1 removed off read 
    if [[ $REPLY =~ [^$@a] ]]; then
      echo "$REPLY is not a valid choice"
    fi
  done
  REPL_RETURN=$REPLY
}

function ittr_list {
  #for i in "$MY_LIST[@]"
  for i in "$2"
    do
      if [ "$i" == $1]; then
        return $1
      fi
    done;
}
