#!/usr/bin/env bash
# Author: Citizen Eight <c8temp@protonmail.com>

function _mkdir {
  mkdir $1
  if [ ! -d "$1" ] || [ ! -w "$1" ]; then
    msg "warn" "cannot write to $1"
  fi
}


# show msg colors
function test_colors {
  msg "G" "this is what green looks like"
  msg "red" "this is what red looks like"
  msg "G" "this is what green looks like"
  msg "B" "this is what blue looks like"
  msg "Y" "this is what yellow looks like"
  msg "title" "this is what a title looks like"
  msg "warn" "this is what a warning looks like"
  msg "cmd" "this is what a command looks like"
  #msg "" "this is what  looks like"
}

function setup {
  source vars.sh
  source util.sh

  # main config dir
  _mkdir ${sits_config_dir}
  cp sits_config/sits_context_val ${sits_config_dir}

  # templates
  _mkdir ${sits_config_dir}/templates
  cp -r sits_config/templates ${sits_config_dir}/

  # default context
  _mkdir ${sits_contexts_dir} 
  cp -r sits_config/contexts/default ${sits_contexts_dir}/

  # library/scripts
  _mkdir ${sits_library_dir} 
  cp sits_config/library/* ${sits_library_dir}

  # main app dir
  if [ "$sits_install_dir" == `pwd` ]; then
    echo "app files already present here"
  else
    _mkdir $sits_install_dir
    cp sits.sh ${sits_install_dir}/
    cp util.sh ${sits_install_dir}/
    cp vars.sh ${sits_install_dir}/
    cp README.md ${sits_install_dir}/
    cp LICENSE ${sits_install_dir}/
  fi
  sleep 1

  msg "B" "=-=-=-="
  msg "B" "\$EDITOR: $EDITOR"
  msg "B" "SitS uses \$EDITOR for editing stored functions. It must be set for this program to function properly"
  msg "B" "to finish intallation:"
  msg "B" "append " "G" $sits_install_dir "B" "to your " "G" PATH "B" " variable"
  msg "B" "optionally, create a short " "G" "alias" "B" " for " "G" "sits.sh" "B" ". For example"
  msg "G" "alias f='source sits.sh'"
  msg "B" "=-=-=-="
}

#  if [ -z $install_flag
setup 
